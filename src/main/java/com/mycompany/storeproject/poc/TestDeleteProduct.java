/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;

/**
 *
 * @author user
 */
public class TestDeleteProduct {
    public static void main(String[] args){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 7);
            int row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :"+ex);
        }
        db.close();
    }
}
